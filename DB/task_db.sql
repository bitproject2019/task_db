-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Mar 29, 2021 at 02:47 AM
-- Server version: 10.3.12-MariaDB
-- PHP Version: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `task_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `email_managers`
--

DROP TABLE IF EXISTS `email_managers`;
CREATE TABLE IF NOT EXISTS `email_managers` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `email` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `batch_id` int(11) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `emailmanagers_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `email_managers`
--

INSERT INTO `email_managers` (`id`, `user_id`, `email`, `name`, `number`, `batch_id`, `created_at`, `updated_at`) VALUES
(1, 1, 'aa@fdf.dfdf', 'sdas', '44', 1, '2021-03-28 12:56:29', '2021-03-28 12:56:29'),
(2, 1, 'sdsd@dfdsf.sdfsdf', 'asdasd', '544485556', 1, '2021-03-28 12:56:29', '2021-03-28 12:56:29'),
(3, 1, 'mathu1152@gmail.com', 'sadsadwerertert', '23', 5, '2021-03-28 13:02:30', '2021-03-28 13:07:22'),
(4, 1, 'mathula2504@gmail.com', 'hema', '578', 5, '2021-03-28 13:02:30', '2021-03-28 13:07:22');

-- --------------------------------------------------------

--
-- Table structure for table `email_schedulers`
--

DROP TABLE IF EXISTS `email_schedulers`;
CREATE TABLE IF NOT EXISTS `email_schedulers` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `alias` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `body` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `batch_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `email_schedulers`
--

INSERT INTO `email_schedulers` (`id`, `user_id`, `alias`, `subject`, `body`, `file`, `date`, `time`, `batch_id`, `created_at`, `updated_at`) VALUES
(1, 1, 'dfsd', 'fsdf', '<p>sdfsfsdg gsdfg</p>', NULL, '2021-03-29', '22:01', '1', '2021-03-28 20:01:08', '2021-03-28 20:01:08'),
(2, 1, 'werw', 'wrwr', '<p>sfsdf sdfsdf</p>', NULL, '2021-03-29', '01:32', '\"1\"', '2021-03-28 20:03:45', '2021-03-28 20:03:45'),
(3, 1, 'hfh', 'fghfgh', '<p>fghfg gh</p>', NULL, '2021-03-29', '01:34', '[\"0\",\"1\"]', '2021-03-28 20:04:13', '2021-03-28 20:04:13'),
(4, 1, 'dsfsdf', 'fdsdfs', '<p>dsfsdf dsfsdf</p>', NULL, '2021-03-29', '01:36', '[\"5\"]', '2021-03-28 20:06:42', '2021-03-28 20:06:42'),
(5, 1, 'dsfsdf', 'fdsdfs', '<p>dsfsdf dsfsdf</p>', NULL, '2021-03-29', '01:36', '[\"5\"]', '2021-03-28 20:07:09', '2021-03-28 20:07:09'),
(6, 1, 'sf', 'sfasfas', '<p>fasfasf</p>', '1.JPG', '2021-03-29', '01:37', '[\"5\"]', '2021-03-28 20:08:56', '2021-03-28 20:08:56'),
(7, 1, 'sf', 'sfasfas', '<p>fasfasf</p>', '1.JPG', '2021-03-29', '01:37', '[\"5\"]', '2021-03-28 20:36:15', '2021-03-28 20:36:15'),
(8, 1, 'sf', 'sfasfas', '<p>fasfasf</p>', '1.JPG', '2021-03-29', '01:37', '[\"5\"]', '2021-03-28 20:37:14', '2021-03-28 20:37:14'),
(9, 1, 'sf', 'sfasfas', '<p>fasfasf</p>', '1.JPG', '2021-03-29', '01:37', '[\"5\"]', '2021-03-28 20:44:56', '2021-03-28 20:44:56'),
(10, 1, 'sf', 'sfasfas', '<p>fasfasf</p>', '1.JPG', '2021-03-29', '01:37', '[\"5\"]', '2021-03-28 20:45:05', '2021-03-28 20:45:05'),
(11, 1, 'sf', 'sfasfas', '<p>fasfasf</p>', '1.JPG', '2021-03-29', '01:37', '[\"5\"]', '2021-03-28 20:45:30', '2021-03-28 20:45:30'),
(12, 1, 'sf', 'sfasfas', '<p>fasfasf</p>', '1.JPG', '2021-03-29', '01:37', '[\"5\"]', '2021-03-28 20:46:24', '2021-03-28 20:46:24'),
(13, 1, 'sf', 'sfasfas', '<p>fasfasf</p>', '1.JPG', '2021-03-29', '01:37', '[\"5\"]', '2021-03-28 20:47:47', '2021-03-28 20:47:47'),
(14, 1, 'sf', 'sfasfas', '<p>fasfasf</p>', '1.JPG', '2021-03-29', '01:37', '[\"5\"]', '2021-03-28 20:47:59', '2021-03-28 20:47:59'),
(15, 1, 'sf', 'sfasfas', '<p>fasfasf</p>', '1.JPG', '2021-03-29', '01:37', '[\"5\"]', '2021-03-28 20:48:06', '2021-03-28 20:48:06'),
(16, 1, 'sf', 'sfasfas', '<p>fasfasf</p>', '1.JPG', '2021-03-29', '01:37', '[\"5\"]', '2021-03-28 20:49:41', '2021-03-28 20:49:41'),
(17, 1, 'sf', 'sfasfas', '<p>fasfasf</p>', '1.JPG', '2021-03-29', '01:37', '[\"5\"]', '2021-03-28 20:50:29', '2021-03-28 20:50:29'),
(18, 1, 'sf', 'sfasfas', '<p>fasfasf</p>', '1.JPG', '2021-03-29', '01:37', '[\"5\"]', '2021-03-28 20:51:02', '2021-03-28 20:51:02'),
(19, 1, 'sf', 'sfasfas', '<p>fasfasf</p>', '1.JPG', '2021-03-29', '01:37', '[\"5\"]', '2021-03-28 20:51:14', '2021-03-28 20:51:14'),
(20, 1, 'sf', 'sfasfas', '<p>fasfasf</p>', '1.JPG', '2021-03-29', '01:37', '[\"5\"]', '2021-03-28 20:51:47', '2021-03-28 20:51:47'),
(21, 1, 'sf', 'sfasfas', '<p>fasfasf</p>', '1.JPG', '2021-03-29', '01:37', '[\"5\"]', '2021-03-28 20:52:03', '2021-03-28 20:52:03'),
(22, 1, 'sf', 'sfasfas', '<p>fasfasf</p>', '1.JPG', '2021-03-29', '01:37', '[\"5\"]', '2021-03-28 20:52:18', '2021-03-28 20:52:18'),
(23, 1, 'sf', 'sfasfas', '<p>fasfasf</p>', '1.JPG', '2021-03-29', '01:37', '[\"5\"]', '2021-03-28 20:52:33', '2021-03-28 20:52:33'),
(24, 1, 'sf', 'sfasfas', '<p>fasfasf</p>', '1.JPG', '2021-03-29', '01:37', '[\"5\"]', '2021-03-28 20:52:56', '2021-03-28 20:52:56'),
(25, 1, 'sf', 'sfasfas', '<p>fasfasf</p>', '1.JPG', '2021-03-29', '01:37', '[\"5\"]', '2021-03-28 20:53:01', '2021-03-28 20:53:01'),
(26, 1, 'sf', 'sfasfas', '<p>fasfasf</p>', '1.JPG', '2021-03-29', '01:37', '[\"5\"]', '2021-03-28 20:53:23', '2021-03-28 20:53:23'),
(27, 1, 'sf', 'sfasfas', '<p>fasfasf</p>', '1.JPG', '2021-03-29', '01:37', '[\"5\"]', '2021-03-28 20:53:34', '2021-03-28 20:53:34'),
(28, 1, 'sf', 'sfasfas', '<p>fasfasf</p>', '1.JPG', '2021-03-29', '01:37', '[\"5\"]', '2021-03-28 20:53:52', '2021-03-28 20:53:52'),
(29, 1, 'sf', 'sfasfas', '<p>fasfasf</p>', '1.JPG', '2021-03-29', '01:37', '[\"5\"]', '2021-03-28 20:55:48', '2021-03-28 20:55:48'),
(30, 1, 'sf', 'sfasfas', '<p>fasfasf</p>', '1.JPG', '2021-03-29', '01:37', '[\"5\"]', '2021-03-28 20:56:17', '2021-03-28 20:56:17'),
(31, 1, 'sf', 'sfasfas', '<p>fasfasf</p>', '1.JPG', '2021-03-29', '01:37', '[\"5\"]', '2021-03-28 20:56:23', '2021-03-28 20:56:23'),
(32, 1, 'sf', 'sfasfas', '<p>fasfasf</p>', '1.JPG', '2021-03-29', '01:37', '[\"5\"]', '2021-03-28 20:56:32', '2021-03-28 20:56:32'),
(33, 1, 'sf', 'sfasfas', '<p>fasfasf</p>', '1.JPG', '2021-03-29', '01:37', '[\"5\"]', '2021-03-28 20:56:48', '2021-03-28 20:56:48'),
(34, 1, 'sf', 'sfasfas', '<p>fasfasf</p>', '1.JPG', '2021-03-29', '01:37', '[\"5\"]', '2021-03-28 20:56:58', '2021-03-28 20:56:58'),
(35, 1, 'sf', 'sfasfas', '<p>fasfasf</p>', '1.JPG', '2021-03-29', '01:37', '[\"5\"]', '2021-03-28 20:57:20', '2021-03-28 20:57:20'),
(36, 1, 'sf', 'sfasfas', '<p>fasfasf</p>', '1.JPG', '2021-03-29', '01:37', '[\"5\"]', '2021-03-28 20:58:05', '2021-03-28 20:58:05'),
(37, 1, 'sf', 'sfasfas', '<p>fasfasf</p>', '1.JPG', '2021-03-29', '01:37', '[\"5\"]', '2021-03-28 21:03:57', '2021-03-28 21:03:57'),
(38, 1, 'sf', 'sfasfas', '<p>fasfasf</p>', '1.JPG', '2021-03-29', '01:37', '[\"5\"]', '2021-03-28 21:05:17', '2021-03-28 21:05:17'),
(39, 1, 'sf', 'sfasfas', '<p>fasfasf</p>', '1.JPG', '2021-03-29', '01:37', '[\"5\"]', '2021-03-28 21:06:11', '2021-03-28 21:06:11'),
(40, 1, 'sf', 'sfasfas', '<p>fasfasf</p>', '1.JPG', '2021-03-29', '01:37', '[\"5\"]', '2021-03-28 21:07:28', '2021-03-28 21:07:28'),
(41, 1, 'sf', 'sfasfas', '<p>fasfasf</p>', '1.JPG', '2021-03-29', '01:37', '[\"5\"]', '2021-03-28 21:09:50', '2021-03-28 21:09:50');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

DROP TABLE IF EXISTS `jobs`;
CREATE TABLE IF NOT EXISTS `jobs` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `queue` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint(3) UNSIGNED NOT NULL,
  `reserved_at` int(10) UNSIGNED DEFAULT NULL,
  `available_at` int(10) UNSIGNED NOT NULL,
  `created_at` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  KEY `jobs_queue_index` (`queue`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2021_03_28_183429_create_email_managers_table', 2),
(5, '2021_03_28_183429_create_email_schedulers_table', 3),
(6, '2021_03_29_014600_create_jobs_table', 4);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'mathula', 'admin@gmail.com', NULL, '$2y$10$/OrBfy2iGhWOB93bfS8d9.4qKPUj/CcBjHb6bHQpPpq932EFUrFEG', NULL, '2021-03-28 09:58:21', '2021-03-28 09:58:21'),
(2, 'mathu', 'user@gmail.com', NULL, '$2y$10$Npocs0xcEcuWoKnt0A4k3eG9.awuwcT1MFVje.jzHCH8xUQ.skmx6', NULL, '2021-03-28 21:16:39', '2021-03-28 21:16:39');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
