 <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{asset ('/backend/dist/img/user2-160x160.jpg') }}" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Admin</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
  
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
         <li><a href="{{ route('index') }}"><i class="fa fa-book"></i> <span>Email Manager</span></a></li>
         <li><a href="{{ route('emailscheduler.create') }}"><i class="fa fa-book"></i> <span>Email Scheduler</span></a></li>
         <li><a href="{{ route('logout') }}"
              onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();"><i class="fa fa-book"></i> <span>Logout</span></a>
        
          <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
              @csrf
          </form>
                                  </li>

      </ul>
    </section>
    <!-- /.sidebar -->