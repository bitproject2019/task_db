@extends('backend::template')

@section('content')
       <section class="content-header">
      <h1>
        Email List
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          

          <div class="box">
            <div class="box-header">
                <a href="{{ route('create') }}"><button class="btn btn-primary" style="float: right">Upload</button></a>
              <h3 class="box-title">All Details</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>ID</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Mobile</th>
                  <th>Batch</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($emails as $email)
                         <tr>
                            <td>{{ $email->id }}</td>
                            <td>{{ $email->name }}
                            </td>
                            <td>{{ $email->email }}</td>
                            <td> {{ $email->number }}</td>
                            <td>{{ $email->batch_id }}</td>
                        </tr>
                    @endforeach
               
                </tbody>
                <tfoot>
                <tr>
                  <th>ID</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Mobile</th>
                  <th>Batch</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    @push('script')
        
        <script>
        $(function () {
            $('#example1').DataTable()
            $('#example2').DataTable({
            'paging'      : true,
            'lengthChange': false,
            'searching'   : false,
            'ordering'    : true,
            'info'        : true,
            'autoWidth'   : false
            })
        })
        </script>
    @endpush
    </section>
@endsection

