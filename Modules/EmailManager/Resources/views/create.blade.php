@extends('backend::template')

@section('content')
   
    <section class="content-header">
      <h1>
        Email Manager
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
       <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Upload Details</h3>
            </div>
            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
            <!-- /.box-header -->
            <!-- form start -->
            <form  method="POST" action="{{ route('emailmanager.store') }}" enctype="multipart/form-data" >
              
              @csrf
              <div class="box-body">
                <div class="form-group">
                    <label for="exampleInputFile">Download Sample Excel : </label>
                    <a href="{{ route('download') }}"><button type="button" class="btn btn-success">Download</button></a>

                </div>
                <div class="form-group">
                  <label for="exampleInputFile">Upload Excel</label>
                  <input type="file" id="exampleInputFile" name="file">

                </div>
                
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="{{ route('index') }}"><button type="button" class="btn btn-danger">Cancel</button></a>

              </div>
            </form>
          </div>
          <!-- /.box -->


        </div>
      </div>
      
      @push('script')
          {{-- <script>
            $('form').on('submit',function(e){
              e.preventDefault();
              
              $.ajax({
                type: "POST",
                url: "{{ route('emailmanager.store') }}",
                data:new FormData(this),
                dataType:'json',
                contentType:false,
                cache:false,
                processData:false,
                success: function (response) {
                  console.log('response')
                }
                error: function (errormessage)
                {
                    console.log('errormessage');
                }
              });
              
            });
          </script> --}}
      @endpush
    </section>
@endsection
