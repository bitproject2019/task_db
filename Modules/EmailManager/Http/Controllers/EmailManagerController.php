<?php

namespace Modules\EmailManager\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\EmailManager\Entities\EmailManager;
use Auth;
use Response;

class EmailManagerController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $emails = EmailManager::whereUserId(Auth::id())->get();
        return view('emailmanager::index',compact('emails'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('emailmanager::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        $filename = $request->file;
        $delimiter = ',';
        if (!file_exists($filename) || !is_readable($filename))
        return 'false';

        $header = null;
        $data = array();
        if (($handle = fopen($filename, 'r')) !== false)
        {
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== false)
            {
                if (!$header)
                    $header = $row;
                else
                    $data[] = array_combine($header, $row);
            }
            fclose($handle);
        }
        // return $data;
        $batch = EmailManager::orderBy('batch_id','desc')->first();
        
        $batch_id = $batch == null ? $batch = 1 :(int)$batch->batch_id +1;
        // return $batch_id;
        foreach ($data as $value) {
            $email = EmailManager::whereEmail($value['Email'])->first();
            if(empty($email))
            {
                $email = new EmailManager();

            }
            
            $email->user_id = Auth::id();
            $email->name = $value['Name'];
            $email->email = $value['Email'];
            $email->number = $value['Number'];
            $email->batch_id = $batch_id;

            $email->save();
        }
        return redirect()->back()->with('success', 'Added Successfully');
        

    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('emailmanager::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('emailmanager::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }

    public function download()
    {
        // $table = Tweet::all();
        $filename = "sample.csv";
        $handle = fopen($filename, 'w+');
        fputcsv($handle, array('Id', 'Name', 'Email', 'Number'));
        fclose($handle);

        $headers = array(
            'Content-Type' => 'text/csv',
        );

        return Response::download($filename, 'sample.csv', $headers);
    }
}
