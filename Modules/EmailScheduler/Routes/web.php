<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Modules\EmailScheduler\Http\Controllers\EmailSchedulerController;

Route::prefix('emailscheduler')->group(function() {
    Route::get('/', 'EmailSchedulerController@index');
    Route::get('/create', 'EmailSchedulerController@create')->name('emailscheduler.create');
    Route::post('/store', 'EmailSchedulerController@store')->name('emailscheduler.store');
    Route::get('/email', function(){
    
        $send_mail = 'test@gmail.com';
    
        dispatch(new App\Jobs\SendEmailJob($send_mail));
    
        dd('send mail successfully !!');
    });
   
});
