@extends('backend::template')

@section('content')
   
    <section class="content-header">
      <h1>
         Email Scheduler
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
       <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Upload Details</h3>
            </div>
            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
            <!-- /.box-header -->
            <!-- form start -->
            {{Form::open(['route'=>['emailscheduler.store'],'class'=>'category','files' => true])}}
              @method('POST')
              @csrf
              <div class="box-body">
                <div class="form-group">
                  <label for="alias">Email Alias</label>
                  {!!Form::text('alias',null,['class' =>'form-control'])!!}
                </div>
                <div class="form-group">
                  <label for="subject">Email Subject</label>
                  {!!Form::text('subject',null,['class' =>'form-control'])!!}
                </div>
                 <div class="form-group">
                  <label for="body">Email Body</label>
                  {!!Form::textarea('body',null,['class' =>'textarea', 'style'=>"width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"])!!}
                   
                </div>
                 <div class="form-group">
                  <label for="file">Email Attachments</label>
                  {!!Form::file('file',null,['class' =>'form-control'])!!}
                </div>

                <div class="form-group">
                  <label for="date">Send Date</label>
                  {!!Form::date('date',\Carbon\Carbon::now(),['class' =>'form-control'])!!}
                  {!! Form::time('time', \Carbon\Carbon::now(), ['class' => 'form-control', 'id'=>'datetimepicker']) !!}
                </div>

                <div class="form-group">
                  <label for="batch_id">Email Batch</label>
                  {!! Form::select('batch_id[]', $batches,null,['class'=>'form-control select2','multiple'=>'multiple','data-placeholder'=>'Select']); !!}
                   
                </div>
                
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="{{ route('index') }}"><button type="button" class="btn btn-danger">Cancel</button></a>

              </div>
            {{Form::close()}}
          </div>
          <!-- /.box -->


        </div>
      </div>
      
      @push('script')
          {{-- <script>
            $('form').on('submit',function(e){
              e.preventDefault();
              
              $.ajax({
                type: "POST",
                url: "{{ route('emailmanager.store') }}",
                data:new FormData(this),
                dataType:'json',
                contentType:false,
                cache:false,
                processData:false,
                success: function (response) {
                  console.log('response')
                }
                error: function (errormessage)
                {
                    console.log('errormessage');
                }
              });
              
            });
          </script> --}}
      @endpush
    </section>
@endsection
