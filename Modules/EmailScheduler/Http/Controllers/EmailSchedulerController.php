<?php

namespace Modules\EmailScheduler\Http\Controllers;

use App\Jobs\SendEmailJob;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Modules\EmailManager\Entities\EmailManager;
use Modules\EmailScheduler\Entities\EmailScheduler;
use File;

class EmailSchedulerController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        return view('emailscheduler::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        $batches = EmailManager::whereUserId(Auth::id())->distinct()->pluck('batch_id','batch_id');
        return view('emailscheduler::create',compact('batches'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        $schedule = new EmailScheduler();
        $schedule->user_id = Auth::id();
        $schedule->alias = $request->alias;
        $schedule->subject = $request->subject;
        $schedule->body = $request->body;
        $schedule->date = $request->date;
        $schedule->time = $request->time;
        $schedule->batch_id = json_encode($request->batch_id);

        if($request->hasfile('file'))
        {
            $image_path = "images/".$request->file;

            if(File::exists($image_path)) {
                File::delete($image_path);

            }
            $file = $request->file('file');
            $name = $file->getClientOriginalName();
            $filename =$name;
            $file->move(public_path('images/'),$filename);
            $schedule->file = $filename;


        }
        $schedule->save();
        $emails = EmailManager::whereUserId(Auth::id())->whereIn('batch_id',$request->batch_id)->pluck('email');
        $names = EmailManager::whereUserId(Auth::id())->whereIn('batch_id',$request->batch_id)->pluck('name');

        SendEmailJob::dispatch($schedule,$emails,$names);
        return redirect()->back()->with('success','Send Successfully');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('emailscheduler::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('emailscheduler::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
}
