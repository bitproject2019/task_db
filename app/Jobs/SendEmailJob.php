<?php

namespace App\Jobs;

use App\Mail\SendEmail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Mail;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

class SendEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $alias;
    protected $subject;
    protected $body;
    protected $file;
    protected $date;
    protected $time;
    protected $batch;
    protected $email;
    protected $name;



    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($sender,$emails,$names)
    {
        $this->send_mail = $sender->alias;
        $this->subject = $sender->subject;
        $this->body = $sender->body;
        $this->file = $sender->file;
        $this->date = $sender->date;
        $this->time = $sender->time;
        $this->batch = $sender->batch;
        $this->email = $emails;
        $this->name = $names;

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $e   = substr($this->email, 1);
        $e = substr($e, 0, -1); 
        $n   = substr($this->name, 1);
        $n = substr($n, 0, -1); 
        $email = explode(',',$e);
        $names = explode(',',$n);
         echo $email[1].'<br>';
         echo $names[1].'<br>';

     
        $mail = new PHPMailer(true);
        $mail->SMTPDebug = SMTP::DEBUG_SERVER;                      //Enable verbose debug output
        $mail->isSMTP();                                            //Send using SMTP
        $mail->Host       ='smtp.gmail.com';                     //Set the SMTP server to send through
        $mail->SMTPAuth   = true;                                   //Enable SMTP authentication
        $mail->Username   = 'mathula2596@gmail.com';                     //SMTP username
        $mail->Password   = 'fzwkpjedsiwqrbxh';                               //SMTP password
        $mail->SMTPSecure = 'tls'; // Enable TLS encryption, `ssl` also accepted

        //Recipients
        for ($i=0; $i < count($email) ; $i++) { 
            $e   = substr($email[$i], 1);
            $e = substr($e, 0, -1); 
            $n   = substr($email[$i], 1);
            $n = substr($n, 0, -1); 
            $mail->addAddress($e, $n);     //Add a recipient
        }
        $mail->setFrom('mathula2596@gmail.com', 'Mathula');
        $mail->addReplyTo('mathula2596@gmail.com', 'Reply Back to Mathula');
    
        $mail->addAttachment('images/'.$this->file, $this->file);    //Optional name

        //Content
        $mail->isHTML(true);                                  //Set email format to HTML
        $mail->Subject = $this->subject;
        $mail->Body    = $this->body;
        $mail->AltBody = $this->alias;
        $mail->send();
        echo 'Message has been sent';
    }
}
